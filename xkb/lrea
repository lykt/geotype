default  partial alphanumeric_keys
xkb_symbols "basic" {

	name[Group1]= "LREA (ge)";
	name[Group2]= "LREA (en)";

	key <TLDE> { [ slash, asciitilde ] };
	key <AE01> { [ 1, exclam ] };
	key <AE02> { [ 2, at ] };
	key <AE03> { [ 3, numbersign ] };
	key <AE04> { [ 4, section ] };
	key <AE05> { [ 5, percent ] };
	key <AE06> { [ 6, asciicircum ] };
	key <AE07> { [ 7, ampersand ] };
	key <AE08> { [ 8, asterisk ] };
	key <AE09> { [ 9, parenleft ] };
	key <AE10> { [ 0, parenright ] };
	key <AE11> { [ minus, question ] };
	key <AE12> { [ equal, plus ] };

	key <AD01> { [ Georgian_zen ], [ z, Z ] };
	key <AD02> { [ Georgian_shin ], [ w, W ] };
	key <AD03> { [ Georgian_un ], [ u, U ] };
	key <AD04> { [ Georgian_xan ], [ h, H ] };
	key <AD05> { [ Georgian_kan ], [ k, K ] };
	key <AD06> { [ Georgian_ban ], [ b, B ] };
	key <AD07> { [ Georgian_don ], [ d, D ] };
	key <AD08> { [ Georgian_gan ], [ g, G ] };
	key <AD09> { [ Georgian_par ], [ p, P ] };
	key <AD10> { [ Georgian_jhan ], [ j, J ] };
	key <AD11> { [ Georgian_ghan, bracketleft ], [ braceleft, bracketleft ] };
	key <AD12> { [ Georgian_zhar, bracketright ], [ braceright, bracketright ] };

	key <AC01> { [ Georgian_las, 0x010020be ], [ l, L ] };
	key <AC02> { [ Georgian_rae ], [ r, R ] };
	key <AC03> { [ Georgian_en, doublelowquotemark ], [ e, E ] };
	key <AC04> { [ Georgian_an, leftdoublequotemark ], [ a, A ] };
	key <AC05> { [ Georgian_on ], [ o, O ] };
	key <AC06> { [ Georgian_tar ], [ t, T ] };
	key <AC07> { [ Georgian_san ], [ s, S ] };
	key <AC08> { [ Georgian_in ], [ i, I ] };
	key <AC09> { [ Georgian_nar ], [ n, N ] };
	key <AC10> { [ Georgian_man ], [ m, M ] };
	key <AC11> { [ Georgian_phar ], [ f, F ] };
	key <AC12> { [ Georgian_jil, bar ], [ quotedbl, bar ] };

	key <LSGT> { [ Georgian_chin, backslash ], [ apostrophe, backslash ] };
	key <AB01> { [ Georgian_char, less ], [ grave, less ] };
	key <AB02> { [ Georgian_qar, greater ], [ dollar, greater ] };
	key <AB03> { [ comma, semicolon ] };
	key <AB04> { [ period, colon ] };
	key <AB05> { [ Georgian_cil ], [ x, X ] };
	key <AB06> { [ Georgian_can ], [ c, C ] };
	key <AB07> { [ Georgian_vin ], [ v, V ] };
	key <AB08> { [ Georgian_tan ], [ y, Y ] };
	key <AB09> { [ Georgian_khar ], [ q, Q ] };
	key <AB10> { [ Georgian_hae, emdash ], [ underscore, emdash ] };

	key <CAPS> { [ ISO_Next_Group ] };

};

// Equilibrial layout
// Bilingual Carpalx optimization for ISO keyboard
// 2020 Giorgi Chavchanidze
// Public domain

partial alphanumeric_keys
xkb_symbols "en" {

	include "lrea(basic)"

	name[Group1]= "LREA (en)";
	name[Group2]= "LREA (ge)";

	key <AD01> { [ z, Z ], [ Georgian_zen ] };
	key <AD02> { [ w, W ], [ Georgian_shin ] };
	key <AD03> { [ u, U ], [ Georgian_un ] };
	key <AD04> { [ h, H ], [ Georgian_xan ] };
	key <AD05> { [ k, K ], [ Georgian_kan ] };
	key <AD06> { [ b, B ], [ Georgian_ban ] };
	key <AD07> { [ d, D ], [ Georgian_don ] };
	key <AD08> { [ g, G ], [ Georgian_gan ] };
	key <AD09> { [ p, P ], [ Georgian_par ] };
	key <AD10> { [ j, J ], [ Georgian_jhan ] };
	key <AD11> { [ braceleft, bracketleft ], [ Georgian_ghan, bracketleft ] };
	key <AD12> { [ braceright, bracketright ], [ Georgian_zhar, bracketright ] };

	key <AC01> { [ l, L ], [ Georgian_las, 0x010020be ] };
	key <AC02> { [ r, R ], [ Georgian_rae ] };
	key <AC03> { [ e, E ], [ Georgian_en, doublelowquotemark ] };
	key <AC04> { [ a, A ], [ Georgian_an, leftdoublequotemark ] };
	key <AC05> { [ o, O ], [ Georgian_on ] };
	key <AC06> { [ t, T ], [ Georgian_tar ] };
	key <AC07> { [ s, S ], [ Georgian_san ] };
	key <AC08> { [ i, I ], [ Georgian_in ] };
	key <AC09> { [ n, N ], [ Georgian_nar ] };
	key <AC10> { [ m, M ], [ Georgian_man ] };
	key <AC11> { [ f, F ], [ Georgian_phar ] };
	key <AC12> { [ quotedbl, bar ], [ Georgian_jil, bar ] };

	key <LSGT> { [ apostrophe, backslash ], [ Georgian_chin, backslash ] };
	key <AB01> { [ grave, less ], [ Georgian_char, less ] };
	key <AB02> { [ dollar, greater ], [ Georgian_qar, greater ] };
	key <AB05> { [ x, X ], [ Georgian_cil ] };
	key <AB06> { [ c, C ], [ Georgian_can ] };
	key <AB07> { [ v, V ], [ Georgian_vin ] };
	key <AB08> { [ y, Y ], [ Georgian_tan ] };
	key <AB09> { [ q, Q ], [ Georgian_khar ] };
	key <AB10> { [ underscore, emdash ], [ Georgian_hae, emdash ] };

};
