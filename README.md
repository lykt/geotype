# Georgian keyboard layouts

Carpalx optimized Georgian layouts.

## [Handlender layout](https://lykt.xyz/kbd/effort/#bmia) — [ხელდასახმარი განლაგება](https://lykt.xyz/kbd/bmia/)

Full carpalx optimization for ISO keyboard.

## [Median layout](https://lykt.xyz/kbd/effort/#dris) — [ორთაშუა განლაგება](https://lykt.xyz/kbd/dris/)

Partial carpalx optimization for ISO keyboard. Asymmetric phonetic layout.

## [Equillibrial layout](https://lykt.xyz/kbd/effort/#lrea) — [თანამასწორი განლაგება](https://lykt.xyz/kbd/lrea/)

Bilingual carpalx optimization for ISO keyboard. Symmetric phonetic layout.

## [Linear layout](https://lykt.xyz/kbd/linear/en/) — [სწორხაზოვანი განლაგება](https://lykt.xyz/kbd/linear/)

Carpalx optimization for ortholinear keyboards. Phonetic layout.

## [Fullgrid layout](https://lykt.xyz/kbd/aiia/en/) — [ია](https://lykt.xyz/kbd/aiia/)

Carpalx optimization for fullgrid ortholinear keyboards. Phonetic layout.
